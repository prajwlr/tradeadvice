from server import app


@app.route('/')
def hello_world():
    return app.send_static_file('index.html')
